#include <stdio.h>
#include "syscalls.h"

int main(int argc, char * argv[]) {
    if(argc != 2)  {
        puts("./getgroup pid");
    } else {
        switch(getgroup(atoi(argv[1]))) {
            case 0: puts("DEFAULT"); break;
            case 1: puts("A"); break;
            case 2: puts("B"); break;
        }
    }
}
