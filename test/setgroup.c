#include <stdio.h>
#include "syscalls.h"

int main(int argc, char * argv[]) {
    if(argc != 3)  {
        puts("./setgroup pid group");
    } else {
        setgroup(atoi(argv[1]), atoi(argv[2]));
    }
}
