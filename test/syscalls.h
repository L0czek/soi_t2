#ifndef __SYSCALLS_GROUPS__
#define __SYSCALLS_GROUPS__

void listproc(void);
int getgroup(int arg);
int setgroup(int arg, int groupId);

#endif
