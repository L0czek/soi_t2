#include <stdio.h>
#include <stdlib.h>
#include <lib.h>
#include <time.h>
#include "syscalls.h"

void p() {
    int i;
    for(i=0; i < 0x1000; ++i);
}

int main(int argc, char* argv[]) {
    int n;
    int i;
    int gr;
    int arg;
    time_t t;
    time_t t2;
    if(argc != 3) {
        puts("./a.out n gr");
    } else {
        n = atoi(argv[1]);
        arg = (int)getpid();
        gr = atoi(argv[2]);
        setgroup(arg, gr);
        for(i=0; i < n; ++i)
            p();
       //printf("passed %d on group B\n", t2-t);
    }
}
