#include <stdio.h>
#include <stdlib.h>
#include <lib.h>
#include <time.h>
#include "syscalls.h"

void p() {
    int i;
    for(i=0; i < 0x1000; ++i);
}

int main(int argc, char* argv[]) {
    int n;
    int i;
    int arg;
    time_t t;
    time_t t2;
    if(argc != 2) {
        puts("./a.out n");
    } else {
        n = atoi(argv[1]);
        arg = (int)getpid();
        setgroup(arg, 1);
        t = time(NULL);
        for(i=0; i < n; ++i)p();
        t2 = time(NULL);
        printf("passed %d on group A\n", t2-t);
        setgroup(arg, 2);
        t = time(NULL);
        for(i=0; i < n; ++i)p();
        t2 = time(NULL);
        printf("passed %d on group B\n", t2-t);
    }
}
