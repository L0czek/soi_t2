#include "syscalls.h"
#include <lib.h>


void listproc(void) {
    message m;
    _syscall(MM, LISTPROC, &m);
}
int getgroup(int arg) {
     message m;
     m.m1_i1 = arg;
    _syscall(MM, GETGROUP, &m);
}
int setgroup(int arg, int groupId) {
     message m;
     m.m1_i1 = arg;
     m.m1_i2 = groupId;
    _syscall(MM, SETGROUP, &m);
}


